"""Задача 1. Даны два списка, нужно вернуть элементы, которые есть в 1-ом списке, но нет во 2-ом."""


def first_list_elements1(list1, list2):
    """
    Решение 1. Решение с дополнительной O(1) по памяти на результат,
    но неоптимальное по времени, O(n^2).
    """
    res = []
    for el in list1:
        if el not in list2:
            res.append(el)
    return res


def first_list_elements2(list1, list2):
    """
    Решение 2. Решение с использованием множеств.
    С условием, что списки содержат неизменяемые элементы.
    O(n) по памяти и O(n) по времени.
   """
    s1 = set(list1)
    s2 = set(list2)
    return s1.difference(s2)


if __name__ == "__main__":
    list1 = [1, 2, 3, 4, 5]
    list2 = [3, 4, 5, 6, 7]
    res1 = first_list_elements1(list1, list2)
    res2 = list(first_list_elements2(list1, list2))
    check_result = res1 == res2 == [1, 2]
    assert check_result
